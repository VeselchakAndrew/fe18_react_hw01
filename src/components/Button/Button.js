import React, {Component} from 'react';
import "./Button.scss"

class Button extends Component {
    render() {
        const {bgColor, children, clickHandler, btnClass} = this.props;
        return (
            <button onClick={clickHandler} className={btnClass} style={{backgroundColor: bgColor, color: "white"}}>{children}</button>
        );
    }
}

export default Button;