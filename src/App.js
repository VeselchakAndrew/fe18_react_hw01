import './App.scss';
import React, {Component} from "react";
import Button from "./components/Button/Button";
import ModalWindow from "./components/ModalWindow/ModalWindow";


class App extends Component {
    state = {
        showFirstModal: false,
        showSecondModal: false
    }

    render() {
        const {showFirstModal, showSecondModal} = this.state;

        const firstModalWindow = {
            headerText: "Do you want to delete this file?",
            mainText: "Once you delete this file, it won’t be possible to undo this action. \n" +
                "Are you sure you want to delete it?",
            bgColor: "#e74c3c",
            showCloseCross: true,
        };


        const secondModalWindow = {
            headerText: "Are you ready for the rock?",
            mainText: "Let's Rock-N-Roll!!!",
            bgColor: "green"
        };

        return (
            <div className="App">
                <Button btnClass="btn" clickHandler={this.showFirstModal} bgColor="#e74c3c">Open first modal</Button>
                <Button btnClass="btn" clickHandler={this.showSecondModal} bgColor="green">Open second modal</Button>
                {showFirstModal &&
                <ModalWindow
                    closeWindow={this.closeModal}
                    {...firstModalWindow}
                    action={
                        <>
                            <Button btnClass="modal_btn" clickHandler={this.closeModal}
                                    bgColor={firstModalWindow.bgColor}>OK</Button>
                            <Button btnClass="modal_btn" clickHandler={this.closeModal}
                                    bgColor={firstModalWindow.bgColor}>Cancel</Button>
                        </>
                    }
                />}


                {showSecondModal &&
                <ModalWindow
                    closeWindow={this.closeModal}
                    {...secondModalWindow}
                    showCloseCross={false}
                    action={
                        <>
                            <Button btnClass="modal_btn" clickHandler={this.closeModal}
                                    bgColor={secondModalWindow.bgColor}>OK</Button>
                            <Button btnClass="modal_btn" clickHandler={this.closeModal}
                                    bgColor={secondModalWindow.bgColor}>Cancel</Button>
                        </>}

                />}
            </div>
        );
    }

    showFirstModal = () => {
        const {showFirstModal} = this.state;
        this.setState({
            showFirstModal: true
        })
    }

    showSecondModal = () => {
        const {showSecondModal} = this.state;
        this.setState({
            showSecondModal: true
        })
    }

    closeModal = () => {
        const {showFirstModal, showSecondModal} = this.state;
        this.setState({
            showFirstModal: false,
            showSecondModal: false,
        })
    }
}

export default App;
