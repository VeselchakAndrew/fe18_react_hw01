import React, {Component} from 'react';
import "./ModalWindow.scss";

class ModalWindow extends Component {
    render() {
        const {headerText, mainText, action, bgColor, showCloseCross, closeWindow} = this.props;
        return (
            <div className="modal_bg" onClick={closeWindow}>
                <div className="modal" style={{backgroundColor: bgColor}} onClick={this.preventCloseModal}>
                    <div className="bg_top">
                        {headerText}
                        {showCloseCross && <div onClick={closeWindow} className="cross"></div>}
                    </div>
                    <div className="bg_main_content">
                        {mainText}
                    </div>
                    <div className="action">{action}</div>
                </div>
            </div>
        );
    }

    preventCloseModal = (e) => {
       e.stopPropagation();
    }
}

export default ModalWindow;